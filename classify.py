from pickle import load
from os import listdir
from csv import reader
from re import findall

from sklearn.feature_extraction.text import CountVectorizer

class SplunkLog():

    def __init__(self,content,label):

        self.raw_content = content
        self.split_content = findall(r'[\w]+',content) #splits by non-alpha
        self.label = label
        self.cluster_label = None

# Parameters

PICKLED_CLASSIFIER_LOCATION = 'trained_classifiers/GNB_50.pkl'
PICKLED_VECTORIZER_LOCATION = 'trained_classifiers/vectorizer_50.pkl'
DATA_ROOT = 'data/'
CONTENT_COLUMN_INDEX = 6

#Import classifier and vectorizer
classifier = load(open(PICKLED_CLASSIFIER_LOCATION,'rb'))
vectorizer = load(open(PICKLED_VECTORIZER_LOCATION,'rb'))

#Import data to classify
splunk_logs = []

for csv_file in listdir(DATA_ROOT):

    for n, row in enumerate(reader(open(DATA_ROOT+csv_file),delimiter=';')):
        if n == 0:
            continue

        content = row[CONTENT_COLUMN_INDEX]
        splunk_logs.append(SplunkLog(content,None))

#Create vectors
print(vectorizer.get_feature_names())
matrix = vectorizer.transform([log.raw_content for log in splunk_logs])

#Classify
results = classifier.predict(matrix.toarray())
print(results)