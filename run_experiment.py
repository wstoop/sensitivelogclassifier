from pickle import load, dump
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import precision_score,recall_score

from collections import Counter

USE_DATA_PERCENTAGE = 1
TRAIN_PERCENTAGE = 0.8
CACHE_FOLDER = 'cache/'
TRAINED_CLASSIFIER_FOLDER = 'trained_classifiers/'

SPLIT_BY_CLUSTERS = False

matrix = load(open(CACHE_FOLDER+'features.pkl','rb'))
target = load(open(CACHE_FOLDER+'target.pkl','rb'))
cluster_labels = load(open(CACHE_FOLDER+'clusters.pkl','rb'))

print(Counter(target))

#Cut off data, otherwise memory problems
nr_of_used_instances = round(len(target) * USE_DATA_PERCENTAGE)
matrix = matrix[:nr_of_used_instances,]
target = target[:nr_of_used_instances]
cluster_labels = cluster_labels[:nr_of_used_instances]

#Divide clusters over subsets
nr_of_items_in_testset = round(len(target) * (1-TRAIN_PERCENTAGE))

cluster_labels_in_testset = []
nr_of_items_in_testset_so_far = 0

for cluster_label, count in sorted(Counter(cluster_labels).items(),key=lambda x:x[1]):
    cluster_labels_in_testset.append(cluster_label)
    nr_of_items_in_testset_so_far += count

    if nr_of_items_in_testset_so_far >= nr_of_items_in_testset:
        break

#Manual overwrite of cluster division
#cluster_labels_in_testset = [1,2,3,5,3,105]

#Split into train and test part
if SPLIT_BY_CLUSTERS:

    train_features = []
    train_target = []

    test_features = []
    test_target = []

    for features,target,cluster_label in zip(matrix.toarray(),target,cluster_labels):

        if cluster_label in cluster_labels_in_testset:
            test_features.append(features)
            test_target.append(target)
        else:
            train_features.append(features)
            train_target.append(target)

else:
    nr_of_train_instances = round(nr_of_used_instances * TRAIN_PERCENTAGE)

    train_features = matrix[:nr_of_train_instances,]
    train_features = train_features.toarray()
    test_features = matrix[nr_of_train_instances:,]
    test_features = test_features.toarray()

    train_target = target[:nr_of_train_instances]
    test_target = target[nr_of_train_instances:]

#Create the classifier
classifier = GaussianNB()
classifier.fit(train_features,train_target)
dump(classifier,open(TRAINED_CLASSIFIER_FOLDER+'GNB.pkl','wb'))

results = classifier.predict(test_features)

print(results)
print(precision_score(test_target,results))
print(recall_score(test_target,results))
