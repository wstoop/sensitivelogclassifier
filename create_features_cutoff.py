from os import listdir
from csv import reader
from re import findall
from random import shuffle
from pickle import dump

from sklearn.feature_extraction.text import CountVectorizer, HashingVectorizer
from sklearn.cluster import DBSCAN

from collections import Counter
from statistics import mean

from fuzzywuzzy import fuzz

class SplunkLog():

    def __init__(self,content,label):

        self.raw_content = content
        self.split_content = findall(r'[\w]+',content) #splits by non-alpha
        self.label = label
        self.cluster_label = None

def get_clusters(items,maximum_intra_cluster_distance):

    db = DBSCAN(eps=maximum_intra_cluster_distance).fit(items)
    labels = [int(label) for label in  db.labels_]
    #n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
    return labels

def show_percentages(counter):
    total = sum(counter.values())

    for label, amount in counter.items():
        print(label,amount,amount/total)

#-------------- Script start

#Parameters
DATA_ROOT = 'data/'
TRAINED_CLASSIFIERS_FOLDER = 'trained_classifiers/'
CACHE_FOLDER = 'cache_cutoff/'

CONTENT_COLUMN_INDEX = 6
LABEL_COLUMN_INDEX = 10

MAXIMUM_INTRA_CLUSTER_DISTANCE = 10
FUZZY_THRESHOLD = 75 #percentage (so 0.5)

#Import the logs
splunk_logs = []
thrown_away = []
counter = Counter()

for csv_file in listdir(DATA_ROOT):

    for n, row in enumerate(reader(open(DATA_ROOT+csv_file),delimiter=';')):
        if n == 0:
            continue

        content = row[CONTENT_COLUMN_INDEX]
        label = row[LABEL_COLUMN_INDEX]

        splunk_logs.append(SplunkLog(content,label))
        counter[label] += 1

show_percentages(counter)
shuffle(splunk_logs)

#Create vectors
vectorizer = CountVectorizer()
matrix = vectorizer.fit_transform([log.raw_content for log in splunk_logs])
target = [log.label in ['y','t'] for log in splunk_logs]
cluster_labels = get_clusters(matrix,maximum_intra_cluster_distance=MAXIMUM_INTRA_CLUSTER_DISTANCE)

#Filter out similar splunk logs
for splunk_log, cluster_label in zip(splunk_logs,cluster_labels):
    splunk_log.cluster_label = cluster_label

cluster_labels_by_size = [x[0] for x in sorted(Counter(cluster_labels).items(),key=lambda x:x[1],reverse=True)]
cluster_labels_by_size.remove(-1)
cluster_labels_by_size.append(-1)

sorted_splunk_logs = []

for cluster_label in cluster_labels_by_size:
    for splunk_log in splunk_logs:
        if splunk_log.cluster_label == cluster_label:
            sorted_splunk_logs.append(splunk_log)

unique_splunk_logs = []
thrown_away_logs = []

for splunk_log in sorted_splunk_logs:
    found_really_similar_one = False

    for splunk_log_already_seen in unique_splunk_logs:
        if fuzz.ratio(splunk_log.raw_content,splunk_log_already_seen.raw_content) > FUZZY_THRESHOLD:
            found_really_similar_one = True
            thrown_away_logs.append(splunk_log)
            print('thrown away',len(thrown_away_logs))
            break

    if not found_really_similar_one:
        unique_splunk_logs.insert(0,splunk_log)
        print('added new unique log',len(unique_splunk_logs))

    if len(unique_splunk_logs) > 20:
        break

#Create vectors of the unique splunk logs

# Old approach
#matrix = vectorizer.fit_transform([log.raw_content for log in unique_splunk_logs])

#New approach
vectorizer = CountVectorizer()
vectorizer.fit([log.raw_content for log in unique_splunk_logs])
feature_names = vectorizer.get_feature_names()
matrix = vectorizer.transform([log.raw_content for log in unique_splunk_logs])

target = [log.label in ['y','t'] for log in unique_splunk_logs]
cluster_labels = get_clusters(matrix,maximum_intra_cluster_distance=MAXIMUM_INTRA_CLUSTER_DISTANCE)

#Save everything
dump(matrix,open(CACHE_FOLDER+'features.pkl','wb'))
dump(target,open(CACHE_FOLDER+'target.pkl','wb'))
dump(cluster_labels,open(CACHE_FOLDER+'clusters.pkl','wb'))
dump(vectorizer,open(TRAINED_CLASSIFIERS_FOLDER+'vectorizer_50.pkl','wb'))