from typing import List

from scipy.spatial.distance import euclidean

from pickle import load

class DBSCAN_distance_matrix():

    maximum_intra_cluster_distance = 5
    minimum_cluster_size = 2

    points : List[int] = []

    visited_points : List[int] = []
    points_already_in_cluster : List[int] = []
    clusters : List[List[int]] = []

    def __init__(self):
        self.points = []
        self.distance_matrix = {}

        self.visited_points = []
        self.points_already_in_cluster = []
        self.clusters = []

    def run(self,points):

        self.points = points

        nr_of_points = len(points)
        for n,point in enumerate(points):

            print(n,nr_of_points)

            if isin(point,self.visited_points):
                continue

            self.visited_points.append(point)
            neighborhood_points = self.region_query(point)

            if len(neighborhood_points) >= self.minimum_cluster_size:
                cluster = []
                self.clusters.append(cluster)
                self.expand_cluster(point, neighborhood_points, cluster)

        return self.clusters

    def calculate_distance(self,point_a,point_b):
        return euclidean(point_a,point_b)

    def region_query(self,base):

        region = []

        for point in self.points:

            if array_equals(point,base):
                region.append(point)
                continue

            if self.calculate_distance(base,point) <= self.maximum_intra_cluster_distance:
                region.append(point)

        return region

    def expand_cluster(self, base, neighborhood_points,cluster):

        cluster.append(base)
        self.points_already_in_cluster.append(base)

        for point in neighborhood_points:

            if not isin (point,self.visited_points):
                self.visited_points.append(point)

                subneighborhood_points = self.region_query(point)

                if len(subneighborhood_points) >= self.minimum_cluster_size:
                    neighborhood_points += subneighborhood_points

            if not isin(point,self.points_already_in_cluster):
                cluster.append(point)
                self.points_already_in_cluster.append(point)

def array_equals(a,b):

    for i,j in zip(a,b):
        if i != j:
            return False

    return True

def isin(a,l):

    for i in l:
        if array_equals(a,i):
            return True

    return False